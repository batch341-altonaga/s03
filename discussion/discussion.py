# Python Lists, Dictionaries, Functions, and Classes

# Lists
# A list is a data structure that is a mutable, or changeable, ordered sequence of elements.
# To create a list, you use square brackets ([])

names = ["John", "Paul", "George", "Ringo"]
programs = ["developer career", "pi-shape", "short courses"]
durations = [260, 180, 20]
truth_values = [True, False, True, True, False]

print(names)
print(durations)
sample_list = ["Apple", 3, False, "Potato", 4, True]
print(sample_list)

# Getting the list size
# The number of elements in a list 
print(len(programs))

# Accessing elements in a list
# You can access elements in a list using their index
print(programs[0])

# Accessing the last element in a list
print(programs[-1])

# Accessing a range of elements in a list
# list_name[start:end] | end index is not included
print(programs[0:2])

# [Section] Mini exercise:
    # 1. Create a list of names of 5 students

    # 2. Create a list of grades for the 5 students
    # 3. Use a loop to iterate through the lists printing in the following format:

students = ["student1", "student2", "student3", "student4", "student5"]
grades = [80, 90, 100, 70, 60]

for i in range(len(students)):
    print(f"The grade of {students[i]} is {grades[i]}")


# [SECTION] Dictionaries
# A dictionary is a data structure that is a mutable, or changeable, unordered sequence of key:value pairs.

person1 = {
    "name": "Daisy",
    "age": 28,
    "occupation": "developer",
    "isEnrolled": True,
    "subjects": ["Python", "SQL", "Django"]
}

print(person1)
# To get the number of key-pairs in a dictionary, use the len() function
print(len(person1))

# Accessing elements in a dictionary
# You can access elements in a dictionary using their key
print(person1["name"])

# keys() method
print(person1.keys()) # returns a list of keys

# values() method
print(person1.values()) # returns a list of values

# items() method
print(person1.items()) # returns a list of key-value pairs

# Adding a new key-value pair to a dictionary
person1["nationality"] = "Filipino"
print(person1)

person1.update({"fav_food" : "Sinigang"})
print(person1) 

# Removing a key-value pair from a dictionary
person1.pop("fav_food")
print(person1) # returns the value of the removed key

del person1["nationality"]
print(person1) # returns nothing

# Removing all key-value pairs from a dictionary
# person1.clear()
# print(person1)

# Removing an array element from a dictionary
# del person1["subjects"][0]

# Looping through a dictionary
for key in person1:
    print(f"The value of {key} is {person1[key]}")

# Nested dictionaries
person3 = {
    "name": "Monica",
    "age": 20,
    "occupation": "poet",
    "isEnrolled": True,
    "subjects": ["Python", "SQL", "Django"],
}

classRoom = {
    "student1": person1,
    "student2": person3,
}
print(classRoom)

# [Section] Mini Exercise
    # 1. Create a car dictionary with the following keys:
    # brand, model, year of make, color
    # 2. Print the following statement from the details:
    # "I own a <Brand> <Model> and it was made in <Year of Make>"

car = {
    "brand": "Chevrolet",
    "model": "Camaro",
    "year of make": 2024,
    "color": "sharkskin metallic"
}

print(f"I own a {car['brand']} {car['model']} and it was made in {car['year of make']}")

# [SECTION] Functions
# Functions are blocks of code the run when called

# "def" keyword is used to create functions
# def <function name>()

def my_greeting():
	# Code to be run when my_greeting is called
	print("Hello, User!")

# Calling/Invoking a function - just specify the function name and provide the values if ever needed
my_greeting()

# Parameters can be added to functions to have more control to what the inputs for the function will be
def greet_user(username):
	print(f"Hello, {username}")

# Arguments are the values that are provided to the function/ substituted to the parameters
greet_user("Bob")
greet_user("Amy")

# Return statement - the "return" keyword allows functions to return values
def addition(num1, num2):
	return num1 + num2

sum = addition(5, 10)
print(f"The sum is {sum}")

# [SECTION] Lambda functions
# A lambda function is a small, anonymous function that can be used for callbacks
# A lambda function can take any number of arguments, but can only have one expression
greeting = lambda person : f"Hello, {person}"
print(greeting("Elsie"))
print(greeting("Anthony"))

mult = lambda a, b : a * b
print(mult(5, 6))
print(mult(6, 99))

# Mini exercise:
# Create a function that gets the square of a number

def sqr(num):
	return num * num

sqr_num = sqr(5)
print(sqr_num)


# [SECTION] Classes
# Classes serve as blueprints to describe the concept of objects
# Each object has characteristics(properties) and behaviors(methods)
# To create a class, the "class" keyword is used along with the class name that starts with an uppecase letter
class Car():
	# Properties that all Car objects must have are defined in the init method
	# Any number of parameters to __init__() can be passed but the first parameter should always be self
	# __init__() is a method that is used to initialize the attributes of an object
	# When you create an instance of a class, the __init__() method is automatically called which allows us to set up the initial states of the object
	def __init__(self, brand, model, year_of_make):
		self.brand = brand
		self.model = model
		self.year_of_make = year_of_make

		# Other properties can be added and assigned hard-coded values
		self.fuel = "Gasoline"
		self.fuel_level = 0

	# methods
	def fill_fuel(self):
		print(f"Current fuel level: {self.fuel_level}")
		print("Filling up the fuel tank...")
		self.fuel_level = 100
		print(f"New fuel level: {self.fuel_level}")

	# Mini-Exercise Solution
	def drive(self, distance):
		print(f"The car has driven {distance} kilometers")
		print(f"The car's fuel level is now: {self.fuel_level - distance}")

# Creating a new instance is done by calling the class and providing the arguments
new_car = Car("Nissan", "GT-R", "2019")

# Displaying attributes can be done using dot notation
print(f"My car is a {new_car.brand} {new_car.model}")

# Calling methods of the instance
new_car.fill_fuel()

# Mini Exercise
# 1. Add a method called drive with a parameter called distance
# 2. The method would output 2 things
# "The car has driven <distance> kilometers"
# "The car's fuel level is <fuel level - distance>"
new_car.drive(50)
