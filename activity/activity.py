# S03 Activity:
# 1. Create a Class called Camper and give it the attributes name, batch, course_type
class Camper:
    def __init__(self, name, batch, course_type):
        self.name = name
        self.batch = batch
        self.course_type = course_type

# 2. Create a method called career_track which will print out the string 'Currently enrolled in the <value of course_type> program' 
    def career_track(self):
        print(f"Currently enrolled in the {self.course_type} program")

# 3. Create a method called info which will print out the string 'My name is <value of name> of batch <value of batch>.' 
    def info(self):
        print(f"My name is {self.name} of batch {self.batch}.")

# 4. Create an object from class Camper called zuitt_camper and pass in arguments for name, batch, and course_type
zuitt_camper = Camper("John Stanley", "Batch 281", "Full Stack Web Development")

# 5. Print the value of the object's name
print(f"Camper Name: {zuitt_camper.name}")

# 6. Print the value of the object's batch
print(f"Camper Batch: {zuitt_camper.batch}")

# 7. Print the value of the object's course type
print(f"Camper Course: {zuitt_camper.course_type}")

# 8. Execute the info method of the object
zuitt_camper.info()

# 9. Execute the career_track method of the object
zuitt_camper.career_track()